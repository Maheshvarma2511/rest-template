package com.antra.demos.model;

import lombok.Data;

@Data
public class Product {
	
	private Integer productId;
	
	private String productName;
	
	private Double price;

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", price=" + price + "]";
	}
	
	

}
