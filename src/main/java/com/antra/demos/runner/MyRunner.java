 package com.antra.demos.runner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.antra.demos.model.Product;

@Component
public class MyRunner implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {
		
	RestTemplate rt = new RestTemplate();
//		
    	Product pr=new Product();
//	    	
//	    	ResponseEntity<List> rs= rt.getForEntity("http://localhost:8080/api/products", List.class);
//	    
//		//Product[] p=(Product[]) rs.getBody();
//		System.out.println(rs.getBody());
//		System.out.println(rs.getStatusCodeValue());
		try {
		pr.setProductId(333);
		pr.setProductName("shirt");
		pr.setPrice(5000.0);		
		ResponseEntity<String> rs1 = rt.postForEntity("http://localhost:8080/api/products/add", pr, String.class);
		System.out.println(rs1.getBody());
		}catch(Exception e) {
			System.out.println(e);
		}
   
	}

}
